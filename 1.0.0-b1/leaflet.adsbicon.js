/* 
 * ADSBIcon: Written by Sarut Puangragsa
 * for RTN ADS-B project
 *  Change log
 *  v.0.4.0 // TODO: Read icon size and change label position accordingly.
 *  v.0.3.1
 *      - Now support icon set. Pattern iconSet + 'icon' + identity.
 *      - Initial dot icon support still need to adjust the label.
 *  v.0.3.0
 *      - now support unlimited type of icon. Just specify the identity and place icon with same name into images folder.
 *      - change file type to gif when State equals 1.
 *  v.0.2.0
 *      - 4 icons are available now.
 *  v.0.1.0
 *      - Well, we are here.
 *      - Added ADSB neutral and ship icon.
 */

function iconSet(symbol, iconStyle) {
    this.symbol = symbol;
    this.iconStyle = iconStyle;
}

var airplaneIconSet = new iconSet("airplane", "icon");
var NTDSIconSet = new iconSet("NTDS", "icon");
var dotIconSet = new iconSet("dot", "dot");
var system_iconSet = airplaneIconSet;

var ADSBIcon = function (Identity, State, iconSet)
{
    if (iconSet === undefined)
    {
        iconSet = system_iconSet;
    }
    if (State === 0)
    {
        return L.icon(
            {
                iconUrl: 'images/'+ iconSet.symbol + '-' + iconSet.iconStyle +'-'+ Identity + '.png',
                iconSize: [32, 32],
                iconAnchor: [16, 16]
            }
        );
    }
    else if (State === 1)
    {
        return L.icon(
            {
                iconUrl: 'images/'+ iconSet.symbol + iconSet.iconStyle + Identity + '.gif',
                iconSize: [32, 32],
                iconAnchor: [16, 16]
            }
        );
    }
};

var ShipIcon = L.icon(
    {
        iconUrl: 'images/ff-icon-black-test-h.png',
        iconSize: [48, 48],
        iconAnchor: [24, 24]
    }
);
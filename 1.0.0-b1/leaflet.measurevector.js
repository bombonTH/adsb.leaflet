/*
 * MeasuringVector: Written by Sarut Puangragsa
 * for RTN ADS-B project
 * Change log
 * v.0.1.0
 * - TODO set onClick method get distance from foot point
 */

function MeasuringVector()
{
    var _latlng = map.getCenter();
    this.Marker = L.marker(_latlng, {
        icon: ShipIcon,
        opacity: 1
    }).addTo(map);
    map.on('mousemove', this._onmousemove, this);
    this.Marker.on('click', function(){alert("clicked")});
    L.DomUtil.disableTextSelection();
    console.log("Cog");
    this._onmousemove = function(e){
        this.Marker.setLatLng(e.latlng);
    }
}

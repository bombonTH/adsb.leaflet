/* 
 * LabelOverlay: Written by Sarut Puangragsa
 * for RTN ADS-B project
 *  Change log
 *  v.0.2.0 TODO more sophisticated zoomAnimation.
 *  v.0.1.0
 *      - Well, we are here.
 *      - Basic functions
 */

L.LabelOverlay = L.Layer.extend(
    {
        initialize: function (latLng, label, options)
        {
            this._latlng = latLng;
            this._label = label;
            L.Util.setOptions(this, options);
        },
        options: {
            offset: new L.Point(0, 2),
            className: 'leaflet-label-overlay'
        },
        onAdd: function (map)
        {
            this._map = map;
            if (!this._container)
            {
                this._initLayout();
            }
            map.getPanes().overlayPane.appendChild(this._container);
            this._container.innerHTML = this._label;
            map.on('viewreset', this._reset, this);
            this._reset();
        },
        onRemove: function (map)
        {
            map.getPanes().overlayPane.removeChild(this._container);
            map.off('viewreset', this._reset, this);
        },
        setData: function (latLng, label)
        {
            this._latlng = latLng;
            this._label = label;
            this._container.innerHTML = this._label;
            this._reset();

        },
        _reset: function ()
        {
            var pos = this._map.latLngToLayerPoint(this._latlng);
            var op = new L.Point(pos.x + this.options.offset.x, pos.y - this.options.offset.y);
            L.DomUtil.setPosition(this._container, op);
        },
        _initLayout: function ()
        {
            this._container = L.DomUtil.create('div', this.options.className);
        }
    }
);
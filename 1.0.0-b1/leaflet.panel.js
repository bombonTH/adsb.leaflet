L.SettingPanel = L.Control.extend(
    {
        options:{
            position: 'topright',
            className: "leaflet-setting-panel",
            title: "Setting Panel"
        },

        onAdd: function (map)
        {
            var container = L.DomUtil.create('div', this.options.className);
            L.DomEvent.disableClickPropagation(container);
            var title  = L.DomUtil.create('div', this.options.className + "-title", container);
            L.DomUtil.create('div', 'panel-separator', container);
            var panel = L.DomUtil.create('div', this.options.className + "-setting", container);
            L.DomUtil.create('div', 'panel-separator', container);
            var button = L.DomUtil.create('div', this.options.className + "-button", container);
            title.innerHTML = this.options.title;
            var ok = L.DomUtil.create('button', 'panel-button', button);
            var cancel = L.DomUtil.create('button', 'panel-button', button);
            var apply = L.DomUtil.create('button', 'panel-button', button);
            ok.innerHTML = ("OK");
            cancel.innerHTML = ("Cancel");
            apply.innerHTML = ("Apply");
            panel.innerHTML = 'Map Opacity:<br> <input class="panel-range" type="range">';
            return container;
        },

        _initLayout: function ()
        {
            this._container = L.DomUtil.create('div', this.options.className);
        }
    }
);

L.panel = function(options){
    return new L.SettingPanel(options);
};
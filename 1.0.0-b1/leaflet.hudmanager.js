L.Control.OwnShipData = L.Control.extend(
    {
        options: {
            position: 'bottomleft',
            emptyString: '<b>Own ship data unavailable! Please check GPS coverage.</b>'
        },

        onAdd: function (map)
        {
            this._container = L.DomUtil.create('div', 'leaflet-control-shipdata');
            L.DomEvent.disableClickPropagation(this._container);
            this._container.innerHTML = this.options.emptyString;
            return this._container;
        },

        setData: function (LatLng, COG, SOG)
        {
            var value = "<b>OwnShip </b>" + "Lat " + Util.formatCoordinate(LatLng.lat, "lat") + " Long " + Util.formatCoordinate(LatLng.lng, "lng") + "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COG " + COG + " SOG " + SOG + " kn";
            this._container.innerHTML = value;
        }

    }
);

L.control.ownshipData = function (options)
{
    return new L.Control.OwnShipData(options);
};

L.Control.PointerPosition = L.Control.extend(
    {
        options: {
            position: 'bottomleft',
            emptyString: 'Unavailable',
            lngFirst: false,
            prefix: "<b>Pointer </b> "
        },

        onAdd: function (map)
        {
            this._container = L.DomUtil.create('div', 'leaflet-control-pointerdata');
            L.DomEvent.disableClickPropagation(this._container);
            map.on('mousemove', this._onMouseMove, this);
            this._container.innerHTML = this.options.emptyString;
            return this._container;
        },

        onRemove: function (map)
        {
            map.off('mousemove', this._onMouseMove)
        },

        _onMouseMove: function (e)
        {
            var value = "Lat " + Util.formatCoordinate(e.latlng.lat, "lat") + " Long " + Util.formatCoordinate(e.latlng.lng, "lng");
            var bearing = ownship.LatLng.bearingTo(e.latlng).toFixed(1);
            var distance = Util.pad((ownship.LatLng.distanceTo(e.latlng) / 1852).toFixed(2), -6);
            var ttg = Util.getTime(ownship.SOG, distance);
            var prefixAndValue = this.options.prefix + value + '<br><b>Bearing</b> ' + Util.pad(bearing, 5) + " - " + distance + " NM <b>TTG</b>: " + L.Util.template(Util.timeTemplates["HH:MM:SS"], ttg); //'<br>' + value +'<br>  <b>Bearing</b> ' + relative + ' <b>Distance</b> '+ distance +' Nm <br> <b>ETA</b> '+eta;
            this._container.innerHTML = prefixAndValue;
        }

    }
);

L.Map.mergeOptions(
    {
        positionControl: false
    }
);

L.Map.addInitHook(
    function ()
    {
        if (this.options.positionControl)
        {
            this.positionControl = new L.Control.MousePosition();
            this.addControl(this.positionControl);
        }
    }
);

L.control.pointerPosition = function (options)
{
    return new L.Control.PointerPosition(options);
};


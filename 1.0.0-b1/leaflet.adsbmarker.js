/*
 * ADSBMarker: Written by Sarut Puangragsa
 * for RTN ADS-B project
 *  Change log
 *  v.0.3.0 TODO add debug flag. Get rid of orientedMarker. set Marker onClick function.
 *  v.0.2.3
 *      - Fix remove function now .remove.
 *      - Streamline all function to get shorter code.
 *      - Implemented HUD panel in leaflet.hudmanager.js.
 *  v.0.2.2
 *      - Fix removeFrom does not completely remove labels.
 *      - Add timeout factor for icon related function. Calculate Rx status base on Timeout value and parsing to icon function
 *  v.0.2.1
 *      - Split ADSBIcon to leaflet.adsbicon.js; will provide icon from there.
 *  v.0.2.0
 *      - Add marker for own ship.
 *      - Streamlining all function.
 *  v.0.1.1
 *      - Remove/Show/Hide ADSB Aircraft and their label.
 *      - Bind all the layer into ADSBLayer LayerGroup for easy control.
 *  v.0.1.0
 *      - Create ADSB Aircrafts with predefined icon with their corresponding label.
 *      - Update ADSB Aircrafts position on function call.
 *      - Padding of Course/IFF data.
 */

var ADSBLabel = L.layerGroup();
var MarkerLayer = L.layerGroup();
var ISR = L.layerGroup();
var OwnShipTrackLayer = L.layerGroup();
var ADSBTrackLayer = L.layerGroup();

var ADSB = {};
var ADSBTrack = {};

function ADSBMarker(ICAO, TN, Flight, Airline, Mode, Squawk, Identity, Altitude, Lat, Lng, Course, Speed, Bearing, Range, Timeout, Comment)
{
    this.LatLng = L.latLng([Lat, Lng]);
    this.ICAO = ICAO;
    this.TN = TN;
    this.Flight = Flight;
    this.Airline = Airline;
    this.Identity = Identity;
    this.Course = Math.round(Course);
    this.Speed = Speed;
    this.Altitude = Altitude;
    this.Bearing = Bearing;
    this.Range = Range;
    this.Squawk1 = "";
    this.Squawk2 = "";
    this.Mode = Mode;
    this.Squawk = Squawk;
    this.Timeout = Timeout;
    this.Comment = Comment;
    this.Marker = new L.orientedMarker(this.LatLng, {icon: ADSBIcon(this.Identity, this.Timeout), angle: this.Course});
    this.TopLabel = new L.LabelOverlay(this.LatLng, this.Flight, {offset: new L.Point(-60, 46), className: 'top-label'});
    this.TopLeftLabel = new L.LabelOverlay(this.LatLng, this.Squawk, {offset: new L.Point(-60, 23), className: 'left-label'});
    this.MidLeftLabel = new L.LabelOverlay(this.LatLng, this.Speed, {offset: new L.Point(-60, 11), className: 'left-label'});
    this.BotLeftLabel = new L.LabelOverlay(this.LatLng, Util.pad(this.Course, 3), {offset: new L.Point(-60, -1), className: 'left-label'});
    this.TopRightLabel = new L.LabelOverlay(this.LatLng, this.ICAO, {offset: new L.Point(20, 23), className: 'right-label'});
    this.MidRightLabel = new L.LabelOverlay(this.LatLng, this.Altitude, {offset: new L.Point(20, 11), className: 'right-label'});
    this.BotRightLabel = new L.LabelOverlay(this.LatLng, this.Airline, {offset: new L.Point(20, -1), className: 'right-label'});
    ADSBLabel.addLayer(this.TopLabel);
    ADSBLabel.addLayer(this.TopLeftLabel);
    ADSBLabel.addLayer(this.MidLeftLabel);
    ADSBLabel.addLayer(this.BotLeftLabel);
    ADSBLabel.addLayer(this.TopRightLabel);
    ADSBLabel.addLayer(this.MidRightLabel);
    ADSBLabel.addLayer(this.BotRightLabel);
    MarkerLayer.addLayer(this.Marker);

    this.Marker.on('click', function(e){
        if (debug) {Console.log("Track " + this.ICAO + " Selected");}
        SelectedTarget(this.ICAO);
    },this);

    this.setData = function (ICAO, TN, Flight, Airline, Mode, Squawk, Identity, Altitude, Lat, Lng, Course, Speed, Bearing, Range, Timeout, Comment)
    {
        this.LatLng = L.latLng([Lat, Lng]);
        this.ICAO = ICAO;
        this.TN = TN;
        this.Flight = Flight;
        this.Airline = Airline;
        this.Identity = Identity;
        this.OldCourse = this.Course;
        this.Course = Math.round(Course);
        this.OldSpeed = this.Speed;
        this.Speed = Math.round(Speed);
        this.OldAltitude = this.Altitude;
        this.Altitude = Altitude;
        this.Bearing = Bearing;
        this.Range = Range;
        this.Mode = Mode;
        this.Squawk = Squawk;
        this.Timeout =Timeout;
        this.Comment = Comment;
        this.TopLabel.setData(this.LatLng, this.Flight);
        this.TopLeftLabel.setData(this.LatLng, Util.pad(this.Squawk, 4));
        this.MidLeftLabel.setData(this.LatLng, Util.checkMomentum(this.OldSpeed, this.Speed));
        this.BotLeftLabel.setData(this.LatLng, Util.pad(Util.checkMomentum(this.OldCourse, this.Course), 3));
        this.TopRightLabel.setData(this.LatLng, this.ICAO);
        this.MidRightLabel.setData(this.LatLng, Util.checkMomentum(this.OldAltitude, this.Altitude));
        this.BotRightLabel.setData(this.LatLng, this.Comment);
        this.Marker.setAngle(this.Course);
        this.Marker.setLatLng(this.LatLng);
        this.Marker.setIcon(ADSBIcon(this.Identity, this.Timeout));
    };

    this.remove = function ()
    {
        MarkerLayer.removeLayer(this.Marker);
        ADSBLabel.removeLayer(this.TopLabel);
        ADSBLabel.removeLayer(this.TopLeftLabel);
        ADSBLabel.removeLayer(this.MidLeftLabel);
        ADSBLabel.removeLayer(this.BotLeftLabel);
        ADSBLabel.removeLayer(this.TopRightLabel);
        ADSBLabel.removeLayer(this.MidRightLabel);
        ADSBLabel.removeLayer(this.BotRightLabel);
        this.marker = [];
        if (debug) console.log("Trying to remove Track number " + this.ICAO);
    }
}

function OwnShipMarker(Lat, Lng, COG, SOG, options )
{
    this.LatLng = L.latLng([Lat, Lng]);
    this.COG = Math.round(COG);
    this.SOG = SOG;
    this.Marker = new L.orientedMarker(this.LatLng, {icon: ShipIcon, angle: this.COG});
    this.Manual = Manual;
    MarkerLayer.addLayer(this.Marker);

    this.setData = function (Lat, Lng, COG, SOG)
    {
        this.LatLng = L.latLng([Lat, Lng]);
        this.OldCOG = this.COG;
        this.COG = Math.round(COG);
        this.OldSOG = this.SOG;
        this.SOG = Math.round(SOG);
        this.Marker.setAngle(this.COG);
        this.Marker.setLatLng(this.LatLng);
    };


}

L.marker.ownShipMarker = function (options)
{
    return new OwnShipMarker(options);
};
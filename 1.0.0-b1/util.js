var Util = [];

Util.coordTemplates = {
    'MinDec': '{degAbs}&deg;{minDec}\'{dir}',
    'DMS': '{degAbs}&deg;{min}\'{sec}"{dir}'
};

Util.timeTemplates = {
    'HH:MM': '{hour}:{min}',
    'HH:MM:SS': '{hour}:{min}:{sec}',
    'Hm': '{hour}h&nbsp;{minDec}m',
    'HMS': '{hour}Hr&nbsp;{min}Min&nbsp;{sec}Sec'
};

Util.getTime = function (Speed, Distance)
{ //Knot, Nautical Mile.
    return L.relTime(Distance / Speed);
};

Util.getSpeed = function (Distance, Time)
{ //Time in hours or L.RelTime object.
    return Distance / (L.relTime(Time).millis / 3600000);
};

Util.getDistance = function (Speed, Time)
{
    return Speed * (L.relTime(Time).millis / 3600000);
};

Util.Decimal2DMS = function (num)
{
    var deg = Math.floor(num);
    var min = ((num - deg) * 60);
    var sec = Math.floor((min - Math.floor(min)) * 60);
    return {
        deg: Util.pad(deg, 3),
        degAbs: Util.pad(Math.abs(deg), 3),
        min: Util.pad(Math.floor(min), 2),
        minDec: Util.pad(min.toFixed(2), 5),
        sec: Util.pad(sec, 2)
    };
};

Util.formatCoordinate = function (coord, axis, style)
{
    if (!style)
    {
        style = 'MinDec';
    }

    if (style == 'decimal')
    {
        var digits;
        if (coord >= 10)
        {
            digits = 2;
        }
        else if (coord >= 1)
        {
            digits = 3;
        }
        else
        {
            digits = 4;
        }
        return coord.toFixed(digits);
    }
    else
    {
        var dms = Util.Decimal2DMS(coord);

        var dir;
        if (dms.deg === 0)
        {
            dir = '&nbsp;';
        }
        else
        {
            if (axis == 'lat')
            {
                dir = (dms.deg > 0 ? 'N' : 'S');
                dms.deg = Util.pad(dms.deg, 2);
                dms.degAbs = Util.pad(dms.degAbs, 2);
            }
            else
            {
                dir = (dms.deg > 0 ? 'E' : 'W');
            }
        }
        return L.Util.template(
            Util.coordTemplates[style],
            L.Util.extend(
                dms, {
                    dir: dir,
                    minDec: dms.minDec
                }
            )
        );
    }
};

Util.pad = function (number, lenght)
{
    var prefix = '0000000000';
    if (lenght < 0)
    {
        return number.substr(0, -lenght);
    }
    return (prefix + number).substr(-lenght);
};

Util.checkMomentum = function (OldValue, NewValue)
{
    if (NewValue > OldValue)
    {
        return NewValue + "?";
    }
    else if (NewValue < OldValue)
    {
        return NewValue + "?";
    }
    return NewValue;
};

Util.deadReckon = function (LatLng, Heading, Speed, Time)
{
    var rad = Math.PI / 180;
    this.LatLng = L.latLng(LatLng);
    this.Advance = Util.getDistance(Speed, L.relTime(Time));
    console.log(this.Advance);
    this.dLat = this.Advance*Math.cos(Heading*rad);
    console.log(this.dLat);
    this.newLat = this.LatLng.lat + this.dLat;
    console.log(this.newLat);
    this.dLng = this.Advance*Math.sin(Heading*rad);
    this.dLng /= Math.cos((this.newLat-this.dLat/2)*rad);
    this.newLng = this.LatLng.lng + this.dLng;
    return L.latLng(this.newLat, this.newLng);
};

L.RelTime = function (hour, min, sec)
{
    if (isNaN(hour) || isNaN(min) || isNaN(sec))
    {
        throw new Error('Invalid Time object: (' + hour + ', ' + min + ', ' + sec + ')');
    }
    this.millis = Math.round(hour * 3600000 + min * 60000 + sec * 1000);
    var timeHolder = Util.Decimal2DMS(this.millis / 3600000);
    this.hour = Util.pad(timeHolder.degAbs, 2);
    this.min = timeHolder.min;
    this.minDec = timeHolder.minDec;
    this.sec = timeHolder.sec;
    this.mil = this.millis % 1000;
};

L.RelTime.prototype = {
    equals: function (obj, millis)
    {
        if (!obj)
        {
            return false;
        }

        obj = L.relTime(obj);

        return Math.abs(this.millis - obj.millis) <= (millis === undefined ? 1000 : millis);
    },

    toString: function ()
    {
        return 'Time ' + this.hour + ':' + this.min + ':' + this.sec + ' - ' + this.millis;
    },

    elapseFrom: function (other)
    {
        return L.relTime().setMillis(Math.abs(this.millis - L.relTime(other).millis));
    },

    setMillis: function (millis)
    {
        this.millis = millis;
        this._update;
    },

    setTime: function (hour, min, sec)
    {
        this.millis = Math.round(hour * 3600000 + min * 60000 + sec * 1000);
        this._update;
    },

    _update: function (millis)
    {
        var timeHolder = Util.Decimal2DMS(this.millis / 3600000);
        this.hour = Util.pad(timeHolder.degAbs, 2);
        this.min = timeHolder.min;
        this.minDec = timeHolder.minDec;
        this.sec = timeHolder.sec;
        this.mil = this.millis % 1000;
    }
};

L.relTime = function (a, b, c)
{
    if (a instanceof L.RelTime)
    {
        return a;
    }
    if (L.Util.isArray(a) && typeof a[0] !== 'object')
    {
        if (a.length === 3)
        {
            return new L.RelTime(a[0], a[1], a[2]);
        }
        if (a.length === 2)
        {
            return new L.RelTime(a[0], a[1], 0);
        }
        return null;
    }
    if (typeof a === 'object' && 'hour' in a && 'min' in a && 'sec' in a)
    {
        return new L.RelTime(a.hour, a.min, a.sec);
    }
    if (a === undefined || a === null)
    {
        a = 0;
    }
    if (b === undefined || b === null)
    {
        b = 0;
    }
    if (c === undefined || c === null)
    {
        c = 0;
    }
    return new L.RelTime(a, b, c);
};